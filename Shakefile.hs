import           Development.Shake
import           Development.Shake.Command
import           Development.Shake.FilePath
import           Development.Shake.Util

markdown = "onesheet.md"
site = "public"
stylec = "style.css"
stylet = "style.tex"
title = "poster"

getCSSStyle = getDirectoryFiles "" [stylec]
getMarkdownFile = getDirectoryFiles "" [markdown]
getTexStyle = getDirectoryFiles "" [stylet]

plaindoc out opts = do
  mdwn <- getMarkdownFile
  need mdwn
  cmd "pandoc" mdwn $ ["-o", out, "-s"] ++ opts

texdoc out opts = do
  style <- getTexStyle
  need style
  plaindoc out $ ["--template", stylet, "--from", "markdown+smart"] ++ opts

main :: IO ()
main = shakeArgs shakeOptions $ do
  let doc  = site </> title ++ ".docx"
  let html = site </> title ++ ".html"
  let pdf  = site </> title ++ ".pdf"
  let rtf  = site </> title ++ ".rtf"

  want [doc, html, pdf, rtf]

  phony "clean" $ do
    putNormal $ "Cleaning files in " ++ site
    removeFilesAfter "." [site]

  doc %> flip plaindoc []

  html %> \out -> do
    css <- getCSSStyle
    need css
    let cssOpts = css >>= (\x -> ["-H", x])
    plaindoc out $ cssOpts ++ ["--to", "html"]

  pdf %> flip texdoc ["-V", "papersize=A4", "--to", "context"]

  rtf %> flip plaindoc []

  phony "doc" $ need [doc]
  phony "html" $ need [html]
  phony "rtf" $ need [rtf]
  phony "pdf" $ need [pdf]
