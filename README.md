# onesheet-template

This is a simple [shake](https://shakebuild.com) project for making simple
glossy pdf posters in markdown and [pandoc](https://pandoc.org/). It
automatically compiles in gitlab's CI to produce an output pdf that can be seen
[here](https://zenhaskell.gitlab.io/onesheet-template/poster.pdf).  It also
produces a .rtf, a .docx, and a .html version.

How to use:

    stack install pandoc shake
    stack exec shake
